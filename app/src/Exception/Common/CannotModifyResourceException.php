<?php

namespace App\Exception\Common;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class CannotModifyResourceException extends AccessDeniedHttpException {

	private const MESSAGE = 'You can\'t modify this resource because you aren\'t the owner';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
