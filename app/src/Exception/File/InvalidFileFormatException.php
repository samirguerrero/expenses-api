<?php

namespace App\Exception\File;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class InvalidFileFormatException extends BadRequestHttpException {
	public const MESSAGE = 'Invalid file format';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
