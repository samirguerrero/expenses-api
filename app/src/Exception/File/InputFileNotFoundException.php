<?php

namespace App\Exception\File;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class InputFileNotFoundException extends BadRequestHttpException {
	public const MESSAGE = '%s input file not found';

	public static function fromFileName($fileName): self {
		throw new self(sprintf(self::MESSAGE, $fileName));
	}
}
