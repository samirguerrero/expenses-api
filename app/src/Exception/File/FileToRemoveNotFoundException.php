<?php

namespace App\Exception\File;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class FileToRemoveNotFoundException extends BadRequestHttpException {
	public const MESSAGE = '%s file to remove not found';

	public static function fromFileName($fileName): self {
		throw new self(sprintf(self::MESSAGE, $fileName));
	}
}
