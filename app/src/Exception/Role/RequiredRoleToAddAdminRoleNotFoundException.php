<?php

namespace App\Exception\Role;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RequiredRoleToAddAdminRoleNotFoundException extends BadRequestHttpException {
	private const MESSAGE = '%s required to perform this operation';

	public static function fromRole(string $role): self {
		throw new self(sprintf(self::MESSAGE, $role));
	}
}
