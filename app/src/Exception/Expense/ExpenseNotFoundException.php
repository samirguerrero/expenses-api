<?php

namespace App\Exception\Expense;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExpenseNotFoundException extends NotFoundHttpException {
	public const MESSAGE = 'The expense is not found';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
