<?php

namespace App\Exception\User;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserIsNotActiveException extends AccessDeniedHttpException {
	private const MESSAGE = 'User with email %s is not longer active, contact with an administrator';

	public static function fromUserEmail($email): self {
		throw new self(sprintf(self::MESSAGE, $email));
	}
}
