<?php

namespace App\Exception\Group;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CannotManageGroupException extends BadRequestHttpException {

	public const MESSAGE = 'You cannot manage this group';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
