<?php

namespace App\Exception\Group;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserIsNotInGroupException extends AccessDeniedHttpException {

	public const MESSAGE = 'The user is not in the group to perform this action';

	public static function create(): self {
		throw new self(self::MESSAGE);
	}
}
