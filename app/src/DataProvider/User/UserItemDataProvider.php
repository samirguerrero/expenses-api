<?php

namespace App\DataProvider\User;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\User;
use App\Repository\UserRepository;

class UserItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface {

	private UserRepository $userRepository;
	private string $staticMediaPath;

	public function __construct(UserRepository $userRepository, string $staticMediaPath) {
		$this->userRepository = $userRepository;
		$this->staticMediaPath = $staticMediaPath;
	}

	/**
	 * @inheritDoc
	 */
	public function supports(string $resourceClass, string $operationName = NULL, array $context = []): bool {
		return $resourceClass === User::class;
	}

	/**
	 * @inheritDoc
	 */
	public function getItem(string $resourceClass, $id, string $operationName = NULL, array $context = []): ?User {
		$user = $this->userRepository->findOneById($id);

		if ($user === null) {
			return null;
		}

		if ($user->getAvatar() === null || $user->getAvatar() === '') {
			$user->setAvatar($this->staticMediaPath . 'dummy-avatar.png');
		}

		return $user;
	}
}
