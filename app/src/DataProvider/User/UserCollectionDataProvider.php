<?php

namespace App\DataProvider\User;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\User;
use App\Repository\UserRepository;
use Generator;

class UserCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface {

	private UserRepository $userRepository;
	private string $staticMediaPath;

	public function __construct(UserRepository $userRepository, string $staticMediaPath) {
		$this->userRepository = $userRepository;
		$this->staticMediaPath = $staticMediaPath;
	}

	/**
	 * @inheritDoc
	 */
	public function supports(string $resourceClass, string $operationName = NULL, array $context = []): bool {
		return $resourceClass === User::class;
	}

	/**
	 * @inheritDoc
	 */
	public function getCollection(string $resourceClass, string $operationName = NULL): Generator {

		$users = $this->userRepository->findAll();
		$dummyAvatarPath = $this->staticMediaPath . 'dummy-avatar.png';

		foreach ($users as $user) {
			// return default avatar file if the user doesn't have an avatar
			if ($user->getAvatar() === null || $user->getAvatar() === '') {
				$user->setAvatar($dummyAvatarPath);
			}
			yield $user;
		}
	}
}
