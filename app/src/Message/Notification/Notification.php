<?php

namespace App\Message\Notification;

class Notification {

	private string $message;

	private array $users;

	public function __construct(string $message, array $users) {
		$this->message = $message;
		$this->users = $users;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string {
		return $this->message;
	}

	/**
	 * @return array
	 */
	public function getUsers(): array {
		return $this->users;
	}
}
