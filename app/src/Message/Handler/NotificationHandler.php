<?php

namespace App\Message\Handler;

use App\Message\Notification\Notification;
use App\Message\NotificationText;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class NotificationHandler implements MessageHandlerInterface {

	public function __invoke(Notification $notification): array {
		$responses = [];
		// send an email for each user given
		foreach ($notification->getUsers() as $user) {
			$mailResult = $this->sendEmail($user, $notification->getMessage());
			array_push($responses, $mailResult);
		}
		return $responses;
	}

	/**
	 * Send an email with a text given
	 *
	 * @param string $user user mail
	 * @param string $text message to send
	 *
	 * @return bool
	 */
	private function sendEmail(string $user, string $text): bool {
		$to = $user;
		$title = NotificationText::$NOTIFICATION_TITLE;
		$message = $text;
		$headers = 'From: exp-api@samsoft.es' . "\r\n" .
		           'Reply-To: no-reply@samsoft.es' . "\r\n" .
		           'X-Mailer: PHP/' . phpversion();
		return mail($to, $title, $message, $headers);
	}
}
