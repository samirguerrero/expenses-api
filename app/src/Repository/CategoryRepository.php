<?php

namespace App\Repository;

use App\Entity\Category;

class CategoryRepository extends BaseRepository {

	protected static function entityClass(): string {
		return Category::class;
	}
}
