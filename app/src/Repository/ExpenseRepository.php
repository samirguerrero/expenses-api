<?php

namespace App\Repository;

use App\Entity\Expense;

class ExpenseRepository extends BaseRepository {

	protected static function entityClass(): string {
		return Expense::class;
	}

	/**
	 * Find an expense by id
	 *
	 * @param string $id
	 *
	 * @return Expense|null
	 */
	public function findOneById(string $id): ?Expense {

		/** @var Expense $expense */
		$expense = $this->objectRepository->find($id);

		return $expense;

	}
}
