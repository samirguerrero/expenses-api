<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;

abstract class BaseRepository {

	protected Connection $connection;

	protected ObjectRepository $objectRepository;

	private ManagerRegistry $managerRegistry;

	public function __construct(ManagerRegistry $managerRegistry, Connection $connection) {
		$this->connection       = $connection;
		$this->managerRegistry  = $managerRegistry;
		$this->objectRepository = $this->getEntityManager()
		                               ->getRepository($this->entityClass());
	}

	/**
	 * Aux method to get or reset an ObjectManager
	 *
	 * @return ObjectManager
	 */
	private function getEntityManager(): ObjectManager {

		$entityManager = $this->managerRegistry->getManager();

		if ($entityManager->isOpen()) {
			return $entityManager;
		}

		return $this->managerRegistry->resetManager();
	}

	/**
	 * To implement by hierarchy classes to give their class name
	 *
	 * @return string Child class name
	 */
	abstract protected static function entityClass(): string;

	public function createQueryBuilder($alias, $indexBy = NULL): QueryBuilder {
		return $this->getEntityManager()->createQueryBuilder();
	}

	protected function saveEntity($entity): void {
		$this->getEntityManager()->persist($entity);
		$this->getEntityManager()->flush();
	}

	protected function removeEntity($entity): void {
		$this->getEntityManager()->remove($entity);
		$this->getEntityManager()->flush();
	}

	/**
	 * Execute a select query and return all results
	 *
	 * @param string $query
	 * @param array  $params
	 *
	 * @return array
	 *
	 * @throws DBALException
	 */
	protected function executeFetchQuery(string $query, array $params = []): array {
		return $this->connection->executeQuery($query, $params)->fetchAll();
	}

	/**
	 * Execute an insert query
	 *
	 * @param string $query
	 * @param array  $params
	 *
	 * @throws DBALException
	 */
	protected function executeInsertQuery(string $query, array $params = []): void {
		$this->connection->executeQuery($query, $params);
	}

}
