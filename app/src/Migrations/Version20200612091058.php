<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200612091058 extends AbstractMigration {
	public function getDescription() : string
	{
		return 'Create expenses table and its relationships';
	}

	public function up(Schema $schema) : void
	{
		$this->addSql('
          create table expense (
				    id char(36) not null primary key,
				    user_id char(36) not null,
				    category_id char(36) not null,
				    group_id char(36) default null,
				    amount decimal(8,2) not null,
				    description tinytext default null,
				    created_at DATETIME NOT NULL,
            updated_at DATETIME NOT NULL,
            index idx_expense_user_id (user_id),
            index idx_expense_group_id (group_id),
            index idx_expense_category_id (category_id),
            constraint fk_expense_user_id 
                foreign key (user_id) references user (id) 
                    on update cascade on delete cascade,
            constraint fk_expense_group_id 
                foreign key (group_id) references user_group (id) 
                    on update cascade on delete cascade,
            constraint fk_expense_category_id 
                foreign key (category_id) references category (id) 
                    on update cascade on delete cascade
					) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE = InnoDB
        ');

	}

	public function down(Schema $schema) : void
	{
		$this->addSql('drop table expense');
	}
}
