<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200615140912 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adds active state on user';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
          alter table user add column active tinyint(1) default 1 after roles 
        ');

    }

    public function down(Schema $schema) : void
    {
	    $this->addSql('
          alter table user drop column active
        ');
    }
}
