<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200608093225 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create category table and its relationships';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
          create table category (
				    id char(36) not null primary key,
				    name varchar(100) not null,
				    user_id char(36) default null,
				    group_id char(36)default  null,
				    created_at DATETIME NOT NULL,
            updated_at DATETIME NOT NULL,
            index idx_category_user_id (user_id),
            index idx_category_group_id (group_id),
            constraint fk_category_user_id 
                foreign key (user_id) references user (id) 
                    on update cascade on delete cascade,
            constraint fk_category_group_id 
                foreign key (group_id) references user_group (id) 
                    on update cascade on delete cascade
					) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE = InnoDB
        ');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('drop table category');
    }
}
