<?php

namespace App\Entity;

use App\Security\Role;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface {

	protected ?string $id = null;

	protected string $name;

	protected string $email;

	protected string $password;

	protected array $roles;

	protected bool $active;

	protected ?string $avatar = null;

	protected DateTime $createdAt;

	protected DateTime $updatedAt;

	/** @var Collection|Group[]  */
	protected ?Collection $groups = null;

	/** @var Collection|Group[]  */
	protected ?Collection $categories = null;

	/** @var Collection|Expense[]  */
	protected ?Collection $expenses = null;

	/**
	 * User constructor.
	 *
	 * @param string      $name
	 * @param string      $email
	 * @param string|null $id
	 *
	 * @throws Exception
	 */
	public function __construct(string $name, string $email, string $id = NULL) {
		$this->id        = $id ?? Uuid::uuid4()->toString();
		$this->name      = $name;
		$this->email     = $email;
		$this->roles     = [Role::ROLE_USER];
		$this->active = false;
		$this->createdAt = new DateTime();
		$this->markAsUpdated();
		$this->groups = new ArrayCollection();
		$this->categories = new ArrayCollection();
		$this->expenses = new ArrayCollection();
	}

	/**
	 * Update the modification date used in lifecycleCallbacks
	 */
	public function markAsUpdated(): void {
		$this->updatedAt = new DateTime();
	}

	/**
	 * Id getter
	 *
	 * @return string|null
	 */
	public function getId(): ?string {
		return $this->id;
	}

	/**
	 * User name getter
	 *
	 * @return string|null
	 */
	public function getName(): ?string {
		return $this->name;
	}

	/**
	 * User name setter
	 *
	 * @param string $name
	 *
	 * @return $this
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}

	/**
	 * User mail getter
	 *
	 * @return string|null
	 */
	public function getEmail(): ?string {
		return $this->email;
	}

	/**
	 * User mail setter
	 *
	 * @param string $email
	 *
	 * @return $this
	 */
	public function setEmail(string $email): self {
		$this->email = $email;
		return $this;
	}

	/**
	 * User password getter
	 *
	 * @return string|null
	 */
	public function getPassword(): ?string {
		return $this->password;
	}

	/**
	 * User password setter
	 *
	 * @param string $password
	 *
	 * @return $this
	 */
	public function setPassword(string $password): self {
		$this->password = $password;
		return $this;
	}

	/**
	 * Roles getter
	 *
	 * @return array|null
	 */
	public function getRoles(): ?array {
		return $this->roles;
	}

	/**
	 * Roles setter
	 * @param array $roles
	 */
	public function setRoles(array $roles): void {
		$this->roles = $roles;
	}

	/**
	 * Creation date getter
	 *
	 * @return \DateTimeInterface|null
	 */
	public function getCreatedAt(): ?DateTimeInterface {
		return $this->createdAt;
	}

	/**
	 * Updated date time
	 *
	 * @return \DateTimeInterface|null
	 */
	public function getUpdatedAt(): ?DateTimeInterface {
		return $this->updatedAt;
	}

	/**
	 * Check if the user given is this user
	 *
	 * @param User $user
	 *
	 * @return bool
	 */
	public function equals(User $user): bool {
		return $this->id === $user->getId();
	}

	/**
	 * @inheritDoc
	 */
	public function getUsername(): string {
		return $this->email;
	}

	/**
	 * @inheritDoc
	 */
	public function getSalt(): void {
	}

	/**
	 * @inheritDoc
	 */
	public function eraseCredentials(): void {
	}

	/**
	 * @return Collection|Group[]
	 */
	public function getGroups(): Collection {
		return $this->groups;
	}

	public function addGroup(Group $group): void {
		$this->groups->add($group);
	}

	public function removeGroup(Group $group) {
		$this->groups->removeElement($group);
	}

	/**
	 * @return Collection|Category[]
	 */
	public function getCategories(): Collection {
		return $this->categories;
	}

	public function addCategory(Category $category): void {
		$this->categories->add($category);
	}

	public function removeCategory(Category $category) {
		$this->categories->removeElement($category);
	}

	/**
	 * @return Collection|Expense[]
	 */
	public function getExpenses(): Collection {
		return $this->expenses;
	}

	/**
	 * @return bool
	 */
	public function isActive(): bool {
		return $this->active;
	}

	/**
	 * @param bool $active
	 */
	public function setActive(bool $active): void {
		$this->active = $active;
	}

	/**
	 * @return string|null
	 */
	public function getAvatar(): ?string {
		return $this->avatar;
	}

	/**
	 * @param string|null $avatar
	 */
	public function setAvatar(?string $avatar): void {
		$this->avatar = $avatar;
	}
}
