<?php

namespace App\Entity;

use DateTime;
use Ramsey\Uuid\Uuid;

class Expense {

	private string $id;
	private User $user;
	private float $amount;
	private ?string $description;
	private ?Category $category;
	private ?Group $group;

	private DateTime $createdAt;
	private DateTime $updatedAt;


	public function __construct(
		Category $category,
		User $user,
		float $amount,
		string $description = null,
		Group $group = null,
		string $id = null) {

		$this->id = $id ?? Uuid::uuid4()->toString();

		$this->createdAt = new DateTime();
		$this->markAsUpdated();

		$this->category = $category;
		$this->user = $user;
		$this->amount = $amount;
		$this->description = $description;
		$this->group = $group;
	}

	/**
	 * @return string
	 */
	public function getId(): string {
		return $this->id;
	}

	/**
	 * @return User|null
	 */
	public function getUser(): ?User {
		return $this->user;
	}

	/**
	 * @return Group|null
	 */
	public function getGroup(): ?Group {
		return $this->group;
	}

	/**
	 * @return DateTime
	 */
	public function getCreatedAt(): DateTime {
		return $this->createdAt;
	}

	public function markAsUpdated(): void {
		$this->updatedAt = new DateTime();
	}

	/**
	 * @return DateTime
	 */
	public function getUpdatedAt(): DateTime {
		return $this->updatedAt;
	}

	public function isOwnedByUser(User $user): bool {
		return $this->getUser()->getId() === $user->getId();
	}

	/**
	 * @return Category|null
	 */
	public function getCategory(): ?Category {
		return $this->category;
	}

	/**
	 * @param Category|null $category
	 */
	public function setCategory(?Category $category): void {
		$this->category = $category;
	}

	/**
	 * @return float
	 */
	public function getAmount(): float {
		return $this->amount;
	}

	/**
	 * @param float $amount
	 */
	public function setAmount(float $amount): void {
		$this->amount = $amount;
	}

	/**
	 * @return string|null
	 */
	public function getDescription(): ?string {
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription(?string $description): void {
		$this->description = $description;
	}
}
