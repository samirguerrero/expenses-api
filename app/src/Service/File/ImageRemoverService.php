<?php

namespace App\Service\File;

use App\Entity\User;
use App\Exception\File\FileToRemoveNotFoundException;
use App\Repository\UserRepository;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use Psr\Log\LoggerInterface;

class ImageRemoverService {

	private UserRepository $userRepository;
	private FilesystemInterface $defaultStorage;
	private string $mediaPath;
	private LoggerInterface $logger;

	public function __construct(
		UserRepository $userRepository,
		FilesystemInterface $defaultStorage,
		string $mediaPath,
		LoggerInterface $logger) {
		$this->userRepository = $userRepository;
		$this->defaultStorage = $defaultStorage;
		$this->mediaPath = $mediaPath;
		$this->logger = $logger;
	}

	/**
	 * Removes an avatar file for the user given and update it in db
	 *
	 * @param User $user
	 *
	 * @return User
	 * @throws FileNotFoundException
	 */
	public function removeAvatar(User $user): User {

		$avatar = $user->getAvatar();

		// the user doesn't have an avatar set it
		if ($avatar === null || $avatar === '') {
			return $user;
		}

		// get avatar file name
		$urlExplode = explode('/', $user->getAvatar());
		$fileName = end($urlExplode);

		// the user doesn't have an avatar set it
		if ($fileName === 'dummy-avatar.png') {
			return $user;
		}

		$response = $this->defaultStorage->delete($fileName);

		// file not found
		if ($response === false) {
			throw FileToRemoveNotFoundException::fromFileName($fileName);
		}

		$user->setAvatar(null);
		$this->userRepository->save($user);

		return $user;
	}

}

