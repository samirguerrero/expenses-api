<?php

declare(strict_types=1);

namespace App\Security\Core\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface, PasswordUpgraderInterface {

	private UserRepository $userRepository;

	public function __construct(UserRepository $userRepository) {
		$this->userRepository = $userRepository;
	}

	/**
	 * Get an user given an username
	 *
	 * @param string $username
	 * @param array  $payload
	 *
	 * @return UserInterface
	 */
	public function loadUserByUserNameAndPayload(string $username, array $payload): UserInterface {
		return $this->findUser($username);
	}

	/**
	 * Find an user by email or throws an UsernameNotFoundException
	 *
	 * @param string $userName
	 *
	 * @return User
	 * @throws UsernameNotFoundException
	 *
	 */
	private function findUser(string $userName): User {
		$user = $this->userRepository->findOneByEmail($userName);

		if ($user === NULL) {
			throw new UsernameNotFoundException(sprintf('User with email: %s not found', $userName));
		}

		return $user;
	}

	/**
	 * @inheritDoc
	 */
	public function refreshUser(UserInterface $user) {
		if (!$user instanceof User) {
			throw new UnsupportedUserException(sprintf('Instance of %s are not supported', get_class($user)));
		}

		return $this->loadUserByUsername($user->getUsername());
	}

	/**
	 * @inheritDoc
	 */
	public function loadUserByUsername(string $username) {
		return $this->findUser($username);
	}

	/**
	 * @inheritDoc
	 */
	public function upgradePassword(UserInterface $user, string $newEncodedPassword): void {
		$user->setPassword($newEncodedPassword);

		$this->userRepository->save($user);
	}

	/**
	 * @inheritDoc
	 */
	public function supportsClass(string $class) {
		return User::class === $class;
	}
}
