<?php

namespace App\Security;

abstract class Role {

	public const ROLE_USER = 'ROLE_USER';
	public const ROLE_ADMIN = 'ROLE_ADMIN';

	/**
	 * Get supported roles
	 *
	 * @return array|string[]
	 */
	public static function getSupportedRoles(): array {
		return [
			self::ROLE_USER,
			self::ROLE_ADMIN
		];
	}
}
