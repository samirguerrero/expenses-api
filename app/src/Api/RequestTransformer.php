<?php

namespace App\Api;

use http\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RequestTransformer {


	public static function transform(Request $request): void{
		$data = json_decode($request->getContent(), true);

		if ($data === null || JSON_ERROR_NONE !== json_last_error()) {
			throw new InvalidArgumentException('Invalid JSON body');
		}

		$request->request->replace($data);
	}

	/**
	 * Get a field from the request given if is an array return a flatten array
	 * 
	 * @param Request $request
	 * @param string $fieldName
	 * @param bool $isArray
	 *
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public static function getRequiredField(Request $request, string $fieldName, bool $isArray = false) {
		$requestData = json_decode($request->getContent(), true);

		if ($isArray) {
			$arrayData = self::arrayFlatten($requestData);

			foreach ($arrayData as $key => $value) {
				if ($fieldName === $key) {
					return $value;
				}
			}

			throw new BadRequestHttpException(sprintf('Missing POST field %s', $fieldName));
		}

		if (array_key_exists($fieldName, $requestData)) {
			return $requestData[$fieldName];
		}

		throw new BadRequestHttpException(sprintf('Missing POST field %s', $fieldName));
	}

	/**
	 * Flatten an array to one level array
	 *
	 * @param array $array
	 *
	 * @return array
	 */
	private static function arrayFlatten(array $array): array {
		$output = [];

		foreach ($array as $key => $value) {
			if(is_array($value)) {
				$output = array_merge($output, self::arrayFlatten($value));
			}
			else {
				$output[$key] = $value;
			}
		}

		return $output;
	}
}
