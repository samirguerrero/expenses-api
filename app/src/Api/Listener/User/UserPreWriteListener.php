<?php

namespace App\Api\Listener\User;

use App\Api\Listener\PreWriteListener;
use App\Api\RequestTransformer;
use App\Entity\User;
use App\Exception\Common\CannotModifyResourceException;
use App\Exception\User\CannotChangeActiveStatusException;
use App\Security\Role;
use App\Security\Validator\Role\RoleValidator;
use App\Service\File\ImageRemoverService;
use App\Service\Password\EncoderService;
use League\Flysystem\FileNotFoundException;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserPreWriteListener implements PreWriteListener {

	private const PUT_USER = 'api_users_put_item';
	private const DELETE_USER = 'api_users_delete_item';
	private const AVATAR_UPLOAD_ROUTE = 'api_users_upload_avatar_item';
	private const AVATAR_REMOVE_ROUTE = 'api_users_remove_avatar_item';

	/** @var iterable|RoleValidator[]  */
	private iterable $roleValidators;
	private EncoderService $encoderService;
	private TokenStorageInterface $tokenStorage;
	private ImageRemoverService $imageRemoverService;

	public function __construct(
		EncoderService $encoderService,
		iterable $roleValidators,
		TokenStorageInterface $tokenStorage,
		ImageRemoverService $imageRemoverService) {
		$this->roleValidators = $roleValidators;
		$this->encoderService = $encoderService;
		$this->tokenStorage = $tokenStorage;
		$this->imageRemoverService = $imageRemoverService;
	}

	/**
	 * @param ViewEvent $event
	 *
	 * @throws FileNotFoundException
	 */
	public function onKernelView(ViewEvent $event): void {

		$request = $event->getRequest();

		// if is an update action then validate fields before persist the data
		if ($request->get('_route') === self::PUT_USER) {

			/** @var User $user */
			$user = $event->getControllerResult();
			/** @var User $tokenUser */
			$tokenUser = $this->tokenStorage->getToken()->getUser();

			// only admins can change active status from an inactive user
			if (!$user->isActive() && !in_array(Role::ROLE_ADMIN, $tokenUser->getRoles())) {
				throw CannotChangeActiveStatusException::create();
			}

			$roles = [];

			// validate roles field with AreValidRole and CanAddRoleAdmin validators
			foreach ($this->roleValidators as $roleValidator) {
				$roles = $roleValidator->validate($request);
			}

			// if roles field is correct set it
			$user->setRoles($roles);

			// get and encode user plain password to set it
			$user->setPassword(
				$this->encoderService->generateEncodedPasswordForUser(
					$user,
					RequestTransformer::getRequiredField($request, 'password')
				)
			);

			return;
		}

		// if is a delete action then remove the avatar file if exists
		if ($request->get('_route') === self::DELETE_USER) {
			/** @var User $user */
			$user = $event->getControllerResult();

			$this->imageRemoverService->removeAvatar($user);

			return;
		}

		// only the avatar owner can remove it or upload it
		if ($request->get('_route') === self::AVATAR_UPLOAD_ROUTE ||
		    $request->get('_route') === self::AVATAR_REMOVE_ROUTE) {
			/** @var User $tokenUser */
			$tokenUser = $this->tokenStorage->getToken()->getUser();

			/** @var User $user */
			$user = $event->getControllerResult();

			// prevent change another user avatar
			if ($tokenUser->getId() !== $user->getId()) {
				throw CannotModifyResourceException::create();
			}
		}

	}

}
