<?php

namespace App\Api\Listener;

use App\Entity\User;
use App\Exception\User\UserIsNotActiveException;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTCreatedListener {
	private RequestStack $requestStack;

	public function __construct(RequestStack $requestStack){
		$this->requestStack = $requestStack;
	}

	/**
	 * Add more data to the JWT
	 *
	 * @param JWTCreatedEvent $event
	 */
	public function onJWTCreated(JWTCreatedEvent $event): void {

		/** @var User $user */
		$user = $event->getUser();

		$createdTime = $user->getCreatedAt()->getTimestamp();
		$updatedTime = $user->getUpdatedAt()->getTimestamp();

		// only in the first login sets active state to the user
		if ($createdTime === $updatedTime) {
			$user->setActive(true);
		}

		// while a user is not active (an admin sets the state to inactive) he can't
		// to do login anymore
		if (!$user->isActive()) {
			throw UserIsNotActiveException::fromUserEmail($user->getEmail());
		}

		// add custom fields
		$payload = $event->getData();
		$payload['name'] = $user->getName();
		$payload['email'] = $user->getEmail();
		$payload['createdAt'] = $user->getCreatedAt();

		$event->setData($payload);
	}
}
