<?php

namespace App\Api\Listener\Expense;

use App\Api\Listener\PreWriteListener;
use App\Entity\Expense;
use App\Entity\Group;
use App\Entity\User;
use App\Exception\Category\CannotAddCategoryException;
use App\Exception\Common\CannotAddAnotherOwnerException;
use App\Exception\Group\UserIsNotInGroupException;
use App\Repository\GroupRepository;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ExpensePreWriteListener implements PreWriteListener {

	private const EXPENSE_POST = 'api_expenses_post_collection';
	private const EXPENSE_PUT = 'api_expenses_put_item';

	private TokenStorageInterface $tokenStorage;
	private GroupRepository $groupRepository;

	public function __construct(TokenStorageInterface $tokenStorage, GroupRepository $groupRepository) {
		$this->tokenStorage = $tokenStorage;
		$this->groupRepository = $groupRepository;
	}

	public function onKernelView(ViewEvent $event): void {

		$request = $event->getRequest();

		// create an expense
		if ($request->get('_route') === self::EXPENSE_POST) {
			/** @var User $tokenUser */
			$tokenUser = $this->tokenStorage->getToken()->getUser();

			/** @var Expense $expense */
			$expense = $event->getControllerResult();

			/** @var Group $group */
			$group = $expense->getGroup();

			// if the expense group exists then we need to check if the current
			// user is member of this group, if the user is the owner or the
			// expense is owned by the group
			if ($group !== null) {

				if (!$this->groupRepository->userIsMember($expense->getGroup(), $tokenUser)) {
					throw UserIsNotInGroupException::create();
				}

				if ($expense->getUser()->getId() !== $tokenUser->getId()) {
					throw CannotAddAnotherOwnerException::create();
				}

				if (!$expense->getCategory()->isOwnedByGroup($group)) {
					throw CannotAddCategoryException::create();
				}

				return;
			}

			// if the login user is the expense owner
			if ($expense->getUser()->getId() !== $tokenUser->getId()) {
				throw CannotAddAnotherOwnerException::create();
			}

			// or the expense is owned by de category user
			if (!$expense->getCategory()->isOwnedBy($tokenUser)) {
				throw CannotAddCategoryException::create();
			}
		}

		// update requests
		if ($request->get('_route') === self::EXPENSE_PUT) {

			/** @var User $tokenUser */
			$tokenUser = $this->tokenStorage->getToken()->getUser();

			/** @var Expense $expense */
			$expense = $event->getControllerResult();

			/** @var Group $group */
			$group = $expense->getGroup();

			// if there is a group
			if ($group !== null) {

				// checks if the category is owned by the same group
				if (!$expense->getCategory()->isOwnedByGroup($group)) {
					throw CannotAddCategoryException::create();
				}

				return;
			}

			// if there is not a group then check if the current logged user is the
			// category owner
			if (!$expense->getCategory()->isOwnedBy($tokenUser)) {
				throw CannotAddCategoryException::create();
			}
		}
	}
}
