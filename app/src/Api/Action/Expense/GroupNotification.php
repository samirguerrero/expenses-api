<?php

namespace App\Api\Action\Expense;

use App\Api\RequestTransformer;
use App\Entity\User;
use App\Exception\Expense\ExpenseNotFoundException;
use App\Message\Notification\Notification;
use App\Repository\ExpenseRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class GroupNotification {

	private MessageBusInterface $messageBus;
	private ExpenseRepository $expenseRepository;

	public function __construct(
		MessageBusInterface $messageBus,
		ExpenseRepository $expenseRepository) {
		$this->messageBus = $messageBus;
		$this->expenseRepository = $expenseRepository;
	}

	/**
	 * Send an email to each users in a group
	 *
	 * @param Request $request
	 * @param string $id
	 *
	 * @return JsonResponse
	 */
	public function __invoke(Request $request, string $id): JsonResponse {

		$text = RequestTransformer::getRequiredField($request, 'text');

		$expense = $this->expenseRepository->findOneById($id);

		// doesn't exist the group with this id
		if ($expense === null) {
			throw ExpenseNotFoundException::create();
		}

		// get the emails
		$usersEmail = array_map(function(User $user){
			return $user->getEmail();
		}, $expense->getGroup()->getUsers()->getValues());

		// enqueue an email to notify the expense changes
		$this->messageBus->dispatch(
			new Notification(
				$text,
				$usersEmail
			)
		);

		return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
	}
}
