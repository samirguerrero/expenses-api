<?php

namespace App\Api\Action\Group;

use App\Api\RequestTransformer;
use App\Entity\User;
use App\Service\Group\GroupService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RemoveUser {

	private GroupService $groupService;

	public function __construct(GroupService $groupService) {
		$this->groupService = $groupService;
	}

	/**
	 * Remove a user from a group
	 *
	 * @param Request $request
	 * @param User $user
	 *
	 * @return JsonResponse
	 *
	 * @throws \Exception
	 */
	public function __invoke(Request $request, User $user): JsonResponse {

		$groupId = RequestTransformer::getRequiredField($request,'group_id');
		$userId = RequestTransformer::getRequiredField($request,'user_id');

		$this->groupService->removeUserFromGroup($groupId, $userId, $user);

		return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
	}
}
