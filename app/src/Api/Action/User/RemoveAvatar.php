<?php

namespace App\Api\Action\User;

use App\Entity\User;
use App\Service\File\ImageRemoverService;
use League\Flysystem\FileNotFoundException;

class RemoveAvatar {

	private ImageRemoverService $imageRemover;

	public function __construct(ImageRemoverService $imageRemover) {
		$this->imageRemover = $imageRemover;
	}

	/**
	 * Remove an avatar file from user given
	 *
	 * @param User $user
	 *
	 * @return User
	 * @throws FileNotFoundException
	 */
	public function __invoke(User $user) {
		return $this->imageRemover->removeAvatar($user);
	}
}
