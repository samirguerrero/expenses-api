<?php

namespace App\Tests\Functional\Api\User;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GetUserCategoriesTest extends UserTestBase {

	/**
	 * Test get owned user categories
	 */
	public function testGetUserCategories(): void {
		$response = $this->makeRequest(self::IDS['user_id']);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertCount(1, $responseData['hydra:member']);
	}

	/**
	 * Test forbidden access (get no data) to another user categories
	 */
	public function testGetAnotherUserCategories(): void {
		$response = $this->makeRequest(self::IDS['admin_id']);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertCount(0, $responseData['hydra:member']);
	}

	/**
	 * Make a get request given an id
	 *
	 * @param string $id
	 *
	 * @return Response
	 */
	private function makeRequest(string $id): Response {
		self::$user->request(
			'GET',
			sprintf(
				'%s/%s/categories.%s',
				$this->endpoint,
				$id,
				self::FORMAT
			)
		);

		return self::$user->getResponse();
	}
}
