<?php

namespace App\Tests\Functional\Api\User;

use App\Security\Role;
use Symfony\Component\HttpFoundation\JsonResponse;

class PutUserTest extends UserTestBase {

	/**
	 * Test change a user with an admin user
	 */
	public function testPutUserWithAdmin(): void {
		$payload = [
			'name' => 'new name',
			'password' => '1234',
			'roles' => [
				Role::ROLE_ADMIN,
				Role::ROLE_USER
			]
		];

		self::$admin->request(
			'PUT',
			sprintf('%s/%s.%s',$this->endpoint, self::IDS['user_id'], self::FORMAT ),
			[], [], [],
			json_encode($payload)
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(
			JsonResponse::HTTP_OK,
			$response->getStatusCode()
		);

		$this->assertEquals(self::IDS['user_id'], $responseData['id']);
		$this->assertEquals($payload['name'], $responseData['name']);
		$this->assertEquals($payload['roles'], $responseData['roles']);

	}

	/**
	 * Test change a user status with an admin user
	 */
	public function testPutUserActiveWithAdmin(): void {
		$payload = [
			'name' => 'new name',
			'password' => '1234',
			'roles' => [
				Role::ROLE_ADMIN,
				Role::ROLE_USER
			],
			'active' => true
		];

		self::$admin->request(
			'PUT',
			sprintf('%s/%s.%s',$this->endpoint, self::IDS['user_no_active_id'], self::FORMAT ),
			[], [], [],
			json_encode($payload)
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(
			JsonResponse::HTTP_OK,
			$response->getStatusCode()
		);

		$this->assertEquals(self::IDS['user_no_active_id'], $responseData['id']);
		$this->assertEquals($payload['name'], $responseData['name']);
		$this->assertEquals($payload['roles'], $responseData['roles']);
		$this->assertEquals($payload['active'], $responseData['active']);

	}

	/**
	 * Test change a user with a wrong role
	 */
	public function testPutUserWithAdminAndWrongRole(): void {
		$payload = [
			'name' => 'new name',
			'password' => '1234',
			'roles' => [
				Role::ROLE_ADMIN,
				'Role_fake'
			]
		];

		self::$admin->request(
			'PUT',
			sprintf('%s/%s.%s',$this->endpoint, self::IDS['user_id'], self::FORMAT ),
			[], [], [],
			json_encode($payload)
		);

		$response = self::$admin->getResponse();

		$this->assertEquals(
			JsonResponse::HTTP_BAD_REQUEST,
			$response->getStatusCode()
		);

	}

	/**
	 * Test forbidden access to add admin role with a user role with a normal user
	 */
	public function testAddAdminRoleWithUser(): void {
		$payload = [
			'name' => 'new name',
			'password' => '1234',
			'roles' => [
				Role::ROLE_ADMIN,
				Role::ROLE_USER
			]
		];

		self::$user->request(
			'PUT',
			sprintf('%s/%s.%s',$this->endpoint, self::IDS['user_id'], self::FORMAT ),
			[], [], [],
			json_encode($payload)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(
			JsonResponse::HTTP_BAD_REQUEST,
			$response->getStatusCode()
		);

	}

	/**
	 * Test forbidden access to add admin role with a user role with a normal user
	 */
	public function testChangeActiveStatusWithUser(): void {
		$payload = [
			'name' => 'new name',
			'password' => '1234',
			'roles' => [
				Role::ROLE_USER
			],
			'active' => false
		];

		self::$noActiveUser->request(
			'PUT',
			sprintf('%s/%s.%s',$this->endpoint, self::IDS['user_no_active_id'], self::FORMAT ),
			[], [], [],
			json_encode($payload)
		);

		$response = self::$noActiveUser->getResponse();

		$this->assertEquals(
			JsonResponse::HTTP_FORBIDDEN,
			$response->getStatusCode()
		);

	}

	/**
	 * Test forbidden access to change a user with a normal user
	 */
	public function testPutAdminWithUser(): void {
		$payload = [
			'name' => 'new name',
			'password' => '1234',
			'roles' => [
				Role::ROLE_ADMIN,
				Role::ROLE_USER
			]
		];

		self::$user->request(
			'PUT',
			sprintf('%s/%s.%s',$this->endpoint, self::IDS['admin_id'], self::FORMAT ),
			[], [], [],
			json_encode($payload)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(
			JsonResponse::HTTP_FORBIDDEN,
			$response->getStatusCode()
		);
	}

}
