<?php

namespace App\Tests\Functional\Api\User;

use App\Tests\Functional\TestBase;
use Doctrine\ORM\Tools\ToolsException;

class UserTestBase extends TestBase {

	protected string $endpoint;

	/**
	 * @throws ToolsException
	 */
	public function setUp(): void {
		parent::setUp();
		$this->endpoint = '/api/v1/users';
	}

}
