<?php

namespace App\Tests\Functional\Api\Expense;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class PutExpenseTest extends ExpenseTestBase {

	/**
	 * Test change an owned expense
	 */
	public function testPutExpense(): void {
		$payload = [
			'category' => sprintf('/api/v1/categories/%s', self::IDS['admin_category_id']),
			'amount' => 1000.20
		];

		$response = $this->makeRequestWithAdmin($payload, self::IDS['admin_category_expense_id']);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertEquals($payload['amount'], $responseData['amount']);
	}

	/**
	 * Test forbidden access to change another user expense
	 */
	public function testPutAnotherUserExpense(): void {
		$payload = [
			'category' => sprintf('/api/v1/categories/%s', self::IDS['user_category_id']),
			'amount' => 1000.20
		];

		$response = $this->makeRequestWithAdmin($payload, self::IDS['admin_category_expense_id']);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Make a put request given a payload and an id
	 *
	 * @param $payload
	 * @param $id
	 *
	 * @return Response
	 */
	private function makeRequestWithAdmin($payload, $id): Response {
		self::$admin->request(
			'PUT',
			sprintf('%s/%s.%s', $this->endpoint, $id, self::FORMAT),
			[],[],[],
			json_encode($payload)
		);

		return self::$admin->getResponse();
	}

}
