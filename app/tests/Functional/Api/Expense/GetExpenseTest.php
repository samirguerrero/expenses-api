<?php

namespace App\Tests\Functional\Api\Expense;

use Symfony\Component\HttpFoundation\JsonResponse;

class GetExpenseTest extends ExpenseTestBase {

	/**
	 * Test get all expenses with an admin user
	 */
	public function testGetExpensesWithAdmin(): void {
		self::$admin->request(
			'GET',
			sprintf('%s.%s', $this->endpoint,self::FORMAT)
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertCount(4, $responseData['hydra:member']);
	}

	/**
	 * Test forbidden access to get all expenses with a normal user
	 */
	public function testGetExpensesWithUser(): void {
		self::$user->request(
			'GET',
			sprintf('%s.%s', $this->endpoint,self::FORMAT)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Test get another user expense with an admin user
	 */
	public function testGetAnotherUserExpensesAsAdmin(): void {
		self::$admin->request(
			'GET',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				self::IDS['user_category_expense_id'],
				self::FORMAT
			)
		);

		$response = self::$admin->getResponse();
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertEquals(self::IDS['user_category_expense_id'], $responseData['id']);
	}

	/**
	 * Test forbidden access to get another user expense
	 */
	public function testGetAnotherUserExpensesAsUser(): void {
		self::$user->request(
			'GET',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				self::IDS['admin_category_expense_id'],
				self::FORMAT
			)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

}
