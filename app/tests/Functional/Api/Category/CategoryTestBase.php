<?php

namespace App\Tests\Functional\Api\Category;

use App\Tests\Functional\TestBase;
use Doctrine\ORM\Tools\ToolsException;

class CategoryTestBase extends TestBase {

	protected string $endpoint;

	/**
	 * @throws ToolsException
	 */
	public function setUp(): void {
		parent::setUp();
		$this->endpoint = '/api/v1/categories';
	}
}
