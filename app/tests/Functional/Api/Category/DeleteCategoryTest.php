<?php

namespace App\Tests\Functional\Api\Category;

use Symfony\Component\HttpFoundation\JsonResponse;

class DeleteCategoryTest extends CategoryTestBase {

	/**
	 * Test delete category
	 */
	public function testDeleteCategory(): void {
		self::$admin->request(
			'DELETE',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				self::IDS['user_category_id'],
			  self::FORMAT
			)
		);

		$response = self::$admin->getResponse();

		$this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->getStatusCode());
	}

	/**
	 * Test forbidden access to delete another user category
	 */
	public function testDeleteAnotherUserCategory(): void {
		self::$user->request(
			'DELETE',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				self::IDS['admin_category_id'],
				self::FORMAT
			)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Test delete category owned by a group
	 */
	public function testDeleteGroupCategory(): void {
		self::$admin->request(
			'DELETE',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				self::IDS['admin_group_category_id'],
				self::FORMAT
			)
		);

		$response = self::$admin->getResponse();

		$this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->getStatusCode());
	}

	/**
	 * Test forbidden access to delete another group category
	 */
	public function testDeleteAnotherGroupCategory(): void {
		self::$user->request(
			'DELETE',
			sprintf(
				'%s/%s.%s',
				$this->endpoint,
				self::IDS['admin_group_category_id'],
				self::FORMAT
			)
		);

		$response = self::$user->getResponse();

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}
}
