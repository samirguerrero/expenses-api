<?php

namespace App\Tests\Functional\Api\Group;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class PutGroupTest extends GroupTestBase {

	private const NAME = 'GroupName';

	/**
	 * Test change an owned group
	 */
	public function testPutGroup(): void {
		$response = $this->makeRequest(self::$admin);
		$responseData = $this->getResponseData($response);

		$this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());
		$this->assertEquals(self::NAME, $responseData['name']);
	}

	/**
	 * Test forbidden access to change another user group
	 */
	public function testPutGroupWithAnotherUser(): void {
		$response = $this->makeRequest(self::$user);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Make a put request given a KernelBrowser object
	 *
	 * @param KernelBrowser $browser
	 *
	 * @return Response
	 */
	private function makeRequest(KernelBrowser $browser): Response {
		$payload = [
			'name' => self::NAME
		];

		$browser->request(
			'PUT',
			sprintf('%s/%s.%s', $this->endPoint, self::IDS['admin_group_id'], self::FORMAT),
			[],
			[],
			[],
			json_encode($payload)
		);

		return $browser->getResponse();
	}
}
