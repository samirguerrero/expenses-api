<?php

namespace App\Tests\Functional\Api\Group;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DeleteGroupTest extends GroupTestBase {

	/**
	 * Test delete an owned group
	 */
	public function testDeleteGroup(): void {

		$response = $this->makeRequest(self::$admin);

		$this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->getStatusCode());
	}

	/**
	 * Test forbidden access to delete another user group
	 */
	public function testDeleteGroupWithAnotherUser(): void {

		$response = $this->makeRequest(self::$user);

		$this->assertEquals(JsonResponse::HTTP_FORBIDDEN, $response->getStatusCode());
	}

	/**
	 * Make a delete request given a KernelBrowser object
	 *
	 * @param KernelBrowser $browser
	 *
	 * @return Response
	 */
	private function makeRequest(KernelBrowser $browser): Response {

		$browser->request(
			'DELETE',
			sprintf(
				'%s/%s.%s',
				$this->endPoint,
				self::IDS['admin_group_id'],
				self::FORMAT
			)
		);

		return $browser->getResponse();
	}
}
