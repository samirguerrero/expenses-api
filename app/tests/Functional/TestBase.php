<?php

namespace App\Tests\Functional;

use App\DataFixtures\AppFixtures;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\ToolsException;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class TestBase extends WebTestCase {
	use FixturesTrait;

	protected const FORMAT = 'jsonld';

	protected const IDS = [
		'admin_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57001',
		'user_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57002',
		'user_no_active_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57013',
		'admin_group_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57003',
		'user_group_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57004',
		'admin_category_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57005',
		'user_category_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57006',
		'admin_group_category_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57007',
		'user_group_category_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57008',
		'admin_category_expense_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57009',
		'user_category_expense_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57010',
		'admin_group_category_expense_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57011',
		'user_group_category_expense_id' => 'f6133ded-46a1-434d-92e3-7a44a3a57010'
	];

	protected static ?KernelBrowser $client = null;
	protected static ?KernelBrowser $user = null;
	protected static ?KernelBrowser $noActiveUser = null;
	protected static ?KernelBrowser $admin = null;

	/**
	 * @throws ToolsException
	 */
	public function setUp(): void {

		if (self::$client === null) {
			self::$client = static::createClient();
		}

		if (self::$admin == null) {
			self::$admin = clone self::$client;
			$this->createAuthenticatedUser(
				self::$admin, 'admin@admin.com', '1234');
		}

		if (self::$user == null) {
			self::$user = clone self::$client;
			$this->createAuthenticatedUser(
				self::$user, 'user@user.com', '5678');
		}

		if (self::$noActiveUser == null) {
			self::$noActiveUser = clone self::$client;
			$this->createAuthenticatedUser(
				self::$noActiveUser, 'user-no-active@user.com', '1234');
		}

		$this->resetDatabase();
	}

	/**
	 * Aux method to transform a response to an array
	 *
	 * @param Response $response
	 *
	 * @return array
	 */
	protected function getResponseData(Response $response): array {
		return json_decode($response->getContent(),true);
	}

	/**
	 * Drop and create the database and load the dummy data
	 *
	 * @throws ToolsException
	 */
	private function resetDatabase (): void {
		/** @var EntityManagerInterface $em */
		$em = $this->getContainer()->get('doctrine')->getManager();

		if (!isset($metadata)) {
			$metadata = $em->getMetadataFactory()->getAllMetadata();
		}

		$schemaTool = new SchemaTool($em);
		$schemaTool->dropDatabase();

		if (!empty($metadata)) {
			$schemaTool->createSchema($metadata);
		}

		$this->loadFixtures([
			AppFixtures::class
		]);

		$this->postFixtureSetup();
	}

	/**
	 * Get and set auth and content type headers to a given client
	 *
	 * @param KernelBrowser $client
	 * @param string $userName
	 * @param string $password
	 */
	private function createAuthenticatedUser(KernelBrowser &$client, string $userName, string $password): void {
		// get auth token
		$client->request(
			'POST',
			'/api/v1/login_check',
			[
				'_email' => $userName,
				'_password' => $password
			]
		);

		$data = json_decode($client->getResponse()->getContent(), true);

		// set client headers
		$client->setServerParameters([
			'HTTP_Authorization' => sprintf('Bearer %s', $data['token']),
			'CONTENT_TYPE' => 'application/json'
		]);
	}
}
