<?php

namespace App\Tests\Unit\Security\Validator\Role;

use App\Exception\Role\RequiredRoleToAddAdminRoleNotFoundException;
use App\Security\Role;
use App\Security\Validator\Role\CanAddRoleAdmin;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class CanAddRoleAdminTest extends TestCase {

	/**
	 * @var Security|MockObject
	 */
	private Security $security;

	private CanAddRoleAdmin $validator;

	public function setUp(): void {
		$this->security =
			$this->getMockBuilder(Security::class)
			     ->disableOriginalConstructor()->getMock();

		$this->validator = new CanAddRoleAdmin($this->security);
	}

	/**
	 * Test when an admin user add admin role to another user
	 */
	public function testCanAddRoleAdmin(): void {
		$payload = [
			'roles' => [
				Role::ROLE_USER,
				Role::ROLE_ADMIN
			]
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$this->security
			->expects(self::exactly(1))
			->method('isGranted')
			->with(Role::ROLE_ADMIN)
			->willReturn(true);

		$response = $this->validator->validate($request);

		$this->assertIsArray($response);
	}

	/**
	 * Test required admin role exception when try to add admin role to an user
	 */
	public function testCanNotAddRoleAdmin(): void {
		$payload = [
			'roles' => [
				Role::ROLE_USER,
				Role::ROLE_ADMIN
			]
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$this->security
			->expects(self::exactly(1))
			->method('isGranted')
			->with(Role::ROLE_ADMIN)
			->willReturn(false);

		$this->expectException(RequiredRoleToAddAdminRoleNotFoundException::class);
		$this->expectExceptionMessage(
			'ROLE_ADMIN required to perform this operation'
		);

		$this->validator->validate($request);
	}
}
