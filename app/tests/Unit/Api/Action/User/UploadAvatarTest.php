<?php

namespace App\Tests\Unit\Api\Action\User;

use App\Api\Action\User\UploadAvatar;
use App\Entity\User;
use App\Service\File\ImageUploaderService;
use App\Tests\Unit\TestBase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class UploadAvatarTest extends TestBase {

	/** @var ImageUploaderService|MockObject */
	private ImageUploaderService $imageUploader;

	private UploadAvatar $action;

	/**
	 * @inheritDoc
	 */
	public function setUp(): void {

		parent::setUp();

		$this->imageUploader =
			$this->getMockBuilder(ImageUploaderService::class)
				->disableOriginalConstructor()->getMock();

		$this->action = new UploadAvatar($this->imageUploader);
	}

	/**
	 * Test imageUploaderService is called
	 *
	 * @throws \Exception
	 */
	public function testUploadAvatar(): void {
		$filePath =
			__DIR__.'/../../../../../src/DataFixtures/test-files/test-image-file.png';
		$file = new UploadedFile($filePath,'test-image-file.png');
		$request = new Request([],[],[],[],[$file]);
		$user = new User('test-user', 'user@mail.com');
		$user->setAvatar('test-image-file.png');

		$this->imageUploader
			->expects(self::exactly(1))
			->method('uploadAvatar')
			->with($request,$user)
			->willReturn($user);

		$response = $this->action->__invoke($request, $user);

		$this->assertEquals($file->getClientOriginalName(), $response->getAvatar());
	}
}
