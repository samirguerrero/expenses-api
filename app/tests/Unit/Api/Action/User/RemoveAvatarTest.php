<?php

namespace App\Tests\Unit\Api\Action\User;

use App\Api\Action\User\RemoveAvatar;
use App\Entity\User;
use App\Service\File\ImageRemoverService;
use App\Tests\Unit\TestBase;
use PHPUnit\Framework\MockObject\MockObject;

class RemoveAvatarTest extends TestBase {

	/** @var ImageRemoverService|MockObject */
	private ImageRemoverService $imageRemover;

	private RemoveAvatar $action;

	/**
	 * @inheritDoc
	 */
	public function setUp(): void {

		parent::setUp();

		$this->imageRemover =
			$this->getMockBuilder(ImageRemoverService::class)
				->disableOriginalConstructor()->getMock();

		$this->action = new RemoveAvatar($this->imageRemover);
	}

	/**
	 * Test imageUploaderService is called
	 *
	 * @throws \Exception
	 */
	public function testRemoveAvatar(): void {

		$user = new User('test-user', 'user@mail.com');
		$user->setAvatar('test-image-file.png');

		$this->imageRemover
			->expects(self::exactly(1))
			->method('removeAvatar')
			->with($user)
			->willReturn($user);

		$user = $this->action->__invoke($user);

		$this->assertNotEmpty($user);

	}
}
