<?php

namespace App\Tests\Unit\Api\Action\User;

use App\Api\Action\User\Register;
use App\Entity\User;
use App\Exception\User\UserAlreadyExistsException;
use App\Service\Password\EncoderService;
use App\Tests\Unit\TestBase;
use DG\BypassFinals;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class RegisterTest extends TestBase {

	/** @var EncoderService|MockObject */
	private EncoderService $encoderService;

	/** @var MessageBusInterface|MockObject  */
	private MessageBusInterface $messageBus;

	private Register $action;

	/**
	 * @inheritDoc
	 */
	public function setUp(): void {

		parent::setUp();

		// static classes init
		BypassFinals::enable();
		
		$this->encoderService =
			$this->getMockBuilder(EncoderService::class)
				->disableOriginalConstructor()->getMock();
		
		$this->messageBus =
			$this->getMockBuilder(MessageBusInterface::class)
			     ->disableOriginalConstructor()->getMock();

		// init the register action
		$this->action =
			new Register(
				$this->userRepository,
				$this->encoderService,
				$this->messageBus
			);
	}

	/**
	 * Test when a first user is registered and his role is admin
	 *
	 * @throws \Exception
	 */
	public function testRegisterFirstUser(): void {

		$payload = [
			'name' => 'name',
			'email' => 'name@mail.com',
			'password' => '1234'
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$this->userRepository
			->expects(self::exactly(1))
			->method('findOneByEmail')
			->with($payload['email'])
			->willReturn(null);

		$this->userRepository
			->expects(self::exactly(1))
			->method('getUsersCount')
			->willReturn(0);

		$this->encoderService
			->expects(self::exactly(1))
			->method('generateEncodedPasswordForUser')
			->with(self::isType('object'),self::isType('string'));

		$this->userRepository
			->expects(self::exactly(1))
			->method('save')
			->with(self::isType('object'));

		$this->messageBus
			->expects(self::exactly(1))
			->method('dispatch')
			->with(self::isType('object'));

		$response = $this->action->__invoke($request);

		$this->assertInstanceOf(User::class, $response);
	}

	/**
	 * Test when there is one or more users in the systems and try to register
	 * a new one and his role is normal user
	 *
	 * @throws \Exception
	 */
	public function testRegisterRestUsers(): void {

		$payload = [
			'name' => 'name',
			'email' => 'name@mail.com',
			'password' => '1234'
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$this->userRepository
			->expects(self::exactly(1))
			->method('findOneByEmail')
			->with(self::isType('string'))
			->willReturn(null);

		$this->userRepository
			->expects(self::exactly(1))
			->method('getUsersCount')
			->willReturn(1);

		$this->encoderService
			->expects(self::exactly(1))
			->method('generateEncodedPasswordForUser')
			->with(self::isType('object'), self::isType('string'));

		$this->userRepository
			->expects(self::exactly(1))
			->method('save')
			->with(self::isType('object'));

		$this->messageBus
			->expects(self::exactly(1))
			->method('dispatch')
			->with(self::isType('object'));

		$response = $this->action->__invoke($request);

		$this->assertInstanceOf(User::class, $response);
	}

	/**
	 * Test when a user is already registered in the system
	 *
	 * @throws \Exception
	 */
	public function testUserAlreadyRegister(): void {

		$payload = [
			'name' => 'name',
			'email' => 'name@mail.com',
			'password' => '1234'
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$user = new User($payload['name'], $payload['email']);

		$this->userRepository
			->expects(self::exactly(1))
			->method('findOneByEmail')
			->with($payload['email'])
			->willReturn($user);

		$this->expectException(UserAlreadyExistsException::class);

		$this->action->__invoke($request);
	}
}
