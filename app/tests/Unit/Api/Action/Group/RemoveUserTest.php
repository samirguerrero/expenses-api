<?php

namespace App\Tests\Unit\Api\Action\User;

use App\Api\Action\Group\RemoveUser;
use App\Entity\User;
use App\Service\Group\GroupService;
use App\Tests\Unit\TestBase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RemoveUserTest extends TestBase {

	private RemoveUser $action;

	/**
	 * @var GroupService|MockObject
	 */
	private GroupService $groupService;

	/**
	 * @inheritDoc
	 */
	public function setUp(): void {

		parent::setUp();

		$this->groupService =
			$this->getMockBuilder(GroupService::class)
			->disableOriginalConstructor()
			->getMock();

		// init the register action
		$this->action =
			new RemoveUser(
				$this->groupService
			);
	}

	/**
	 * Test remove user from group action
	 *
	 * @throws \Exception
	 */
	public function testRemoveUser(): void {

		$payload = [
			'group_id' => 'group_id_01',
			'user_id' => 'new_user_id_01'
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$user = new User('old_user', 'old_user@mail.com');

		// no user found in the system
		$this->groupService
			->expects(self::exactly(1))
			->method('removeUserFromGroup')
			->with($payload['group_id'], $payload['user_id'], $user);

		$response = $this->action->__invoke($request, $user);

		$this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->getStatusCode());
	}
}
