<?php

namespace App\Tests\Unit\Api\Action\Expense;

use App\Api\Action\Expense\GroupNotification;
use App\Entity\Category;
use App\Entity\Expense;
use App\Entity\Group;
use App\Entity\User;
use App\Exception\Expense\ExpenseNotFoundException;
use App\Repository\ExpenseRepository;
use App\Tests\Unit\TestBase;
use DG\BypassFinals;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;

class GroupNotificationTest extends TestBase {

	private GroupNotification $action;

	/**
	 * @var MessageBusInterface|MockObject
	 */
  private MessageBusInterface $messageBus;

	/**
	 * @var ExpenseRepository|MockObject
	 */
  private ExpenseRepository $expenseRepository;

	/**
	 * @inheritDoc
	 */
	public function setUp(): void {
		parent::setUp();

		// static classes init
		BypassFinals::enable();

		$this->messageBus =
			$this->getMockBuilder(MessageBusInterface::class)
			->disableOriginalConstructor()
			->getMock();

		$this->expenseRepository =
			$this->getMockBuilder(ExpenseRepository::class)
			     ->disableOriginalConstructor()
			     ->getMock();

		// init the register action
		$this->action =
			new GroupNotification(
				$this->messageBus,
				$this->expenseRepository
			);
	}

	/**
	 * Happy path
	 *
	 * @throws \Exception
	 */
	public function testGroupNotification(): void {

		$payload = [
			'text' => 'email body test'
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		$user = new User('admin', 'admin@admin.com');
		$category = new Category('adminCategory');
		$amount = 100.0;
		$group = new Group('adminGroup', $user);
		$expense = new Expense($category, $user, $amount, 'description',$group, self::IDS['admin_group_category_expense_id']);

		$this->expenseRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with(self::IDS['admin_group_category_expense_id'])
			->willReturn($expense);

		$this->messageBus
			->expects(self::exactly(1))
			->method('dispatch')
			->with(self::isType('object'));

		$response = $this->action->__invoke($request, self::IDS['admin_group_category_expense_id']);

		$this->assertEquals(JsonResponse::HTTP_NO_CONTENT, $response->getStatusCode());
	}

	/**
	 * Wrong id test
	 *
	 * @throws \Exception
	 */
	public function testExpenseNotFoundException(): void {

		$payload = [
			'text' => 'email body test'
		];

		$request = new Request(
			[], [], [], [], [], [],
			json_encode($payload)
		);

		// no user found in the system
		$this->expenseRepository
			->expects(self::exactly(1))
			->method('findOneById')
			->with(self::IDS['admin_group_category_expense_id'])
			->willReturn(null);

		$this->expectException(ExpenseNotFoundException::class);

		$this->action->__invoke($request, self::IDS['admin_group_category_expense_id']);
	}
}
