<?php

use App\Entity\User;
use App\Exception\File\InputFileNotFoundException;
use App\Exception\File\InvalidFileFormatException;
use App\Service\File\ImageUploaderService;
use App\Tests\Unit\TestBase;
use DG\BypassFinals;
use League\Flysystem\FilesystemInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ImageUploaderServiceTest extends TestBase {

	private ImageUploaderService $imageUploader;

	private FilesystemInterface $defaultStorage;

	private string $mediaPath;

	private User $user;

	public function setUp(): void {
		parent::setUp();

		// static classes init
		BypassFinals::enable();

		$this->defaultStorage =
			$this->getMockBuilder(FilesystemInterface::class)
			->disableOriginalConstructor()
			->getMock();

		/** @var LoggerInterface|MockObject $logger */
		$logger =
			$this->getMockBuilder(LoggerInterface::class)
			     ->disableOriginalConstructor()
			     ->getMock();

		$this->mediaPath = __DIR__ . '/test-media-path';

		$this->imageUploader =
			new ImageUploaderService(
				$this->userRepository,
				$this->defaultStorage,
				$this->mediaPath,
				$logger
			);

		$this->user = new User('test-user', 'user@mail.com');
	}

	public function testInputFileNotFound(): void {

		$request = new Request([],[],[],[],[]);

		$this->expectException(InputFileNotFoundException::class);

		$this->imageUploader->uploadAvatar($request, $this->user);

	}

	public function testWrongFileFormat(): void {

		$filePath = __DIR__ . '/../../../../src/DataFixtures/test-files/test-txt-file.txt';
		$file = new UploadedFile($filePath,'test-txt-file.txt');
		$request = new Request([],[],[],[],['avatar' => $file]);

		$this->expectException(InvalidFileFormatException::class);

		$this->imageUploader->uploadAvatar($request, $this->user);

	}

	public function testSuccessAvatarUpload(): void {

		$filePath = __DIR__ . '/../../../../src/DataFixtures/test-files/test-image-file.png';
		$file = new UploadedFile($filePath,'test-image-file.png');
		$request = new Request([],[],[],[],['avatar' => $file]);

		$this->defaultStorage
			->expects(self::exactly(1))
			->method('writeStream')
			->with(
				$this->isType('string'),
				$this->isType('resource'),
				$this->isType('array')
			);

		$this->userRepository
			->expects(self::exactly(1))
			->method('save')
			->with($this->isType('object'));

		$this->user = $this->imageUploader->uploadAvatar($request, $this->user);
		$this->assertNotNull($this->user->getAvatar());
	}
}
