# EXAMPLE EXPENSES API

This project is an example of how create an API with Symfony 5 and API Platform, Docker and Compose and RabbitMQ queues to
send emails in background tasks.

## PROJECT SECTIONS

There are three main parts: Host computer, docker server infrastructure and code.

### HOST COMPUTER

Maybe is a local machine or a remote server, in this case we use a local machine to local development, and a vps host
to deploy the code in a production environment.

So we use linux as OS and use Nginx with PHP7.4-FPM to be able to use it like a tunnel to the docker servers
or operate with Composer (other wonder php tool to manage repositories) without connect thorough the php server to do 
things like an update or whatever related with PHP.

**IMPORTANT NOTE:**

In the **production environment** a script run every 12h to reset the project to the initial state, **all data will be removed** 
in this period, this is because is an example project and every body can use it, this means a lot of data, and 
a lot of waste of hard disk memory. 

### DOCKER SERVERS

Docker is a great tool to work isolated and at the same time we have the same configuration in some computer where we have installed the code, 
because of that we've chosen this software (it has a lot of advantages, if you want to know more about them, check this link 
[link to Docker page](https://www.docker.com/) ).

Docker has a kind of container orchestrator, Docker compose, that is a great tool too, it allows you to configure easily your
servers or containers with a simple yml file, you can check their documentation in [Docker Compose link](https://docs.docker.com/compose/)

In this project we have these servers or containers:

- Nginx: A web server (sf5-expenses-api-web)
- PHP7.4-FPM to get work PHP, this container is the back end (sf5-expenses-api-be)
- Other PHP7.4 container that consume the queues in the background to send notifications (sf5-expenses-api-php-consumer)
- RabbitMQ server, this is the server that manage the queues (sf5-expenses-api-rabbitmq)
- MailHog server, this server is the example mail server and is use it only for testing purposes, remember that this is an example project (sf5-expenses-api-mailhog)
- MySql server to brings a db for our project (sf5-expenses-api-db)

### CODE

In this project you can find an API made it with Symfony 5.* (PHP framework) and API Platform (with Swagger doc), JWT (auth),
League flysystem (file system control), Ramsey UUID (unique ids) and Doctrine (ORM). 
In testing area we've used PHPUnit framework. Don't you know about Symfony and company?,
[check this link](https://symfony.com/).

This code try to follow 'Good practices' as [SOLID](https://es.wikipedia.org/wiki/SOLID). If you don't follow it, you should it,
try to not waste your time, or the other people time with horrifying spaghetti code :)

## TO GET IT WORKS IN LOCAL

### LOCAL REQUIREMENTS

- A computer with Docker, Docker compose and git installed
- Nginx or other web server to use domain names instead of ugly ips (optional but desired)

### INSTALL THE PROJECT 

First of all you need to download the project to your local machine with Git, you can do it in a terminal:

```git clone https://gitlab.com/samirguerrero/expenses-api.git```

So, you have the code, now go to the docker folder (sf5_exp_api-docker) and run:

(Bash example)

```U_ID=${UID} docker-compose build```

```U_ID=${UID} docker-compose compose up -d```

The first time you need to run doctrine migrations, navigate to symfony project folder (app/) and run:

(development db)

```bin/console d:m:m -n```

(test db)

```bin/console d:m:m -n --env="test"```

If you need dummy data you can load fixtures running:

```bin/console d:f:l -n --env="test"```

or in the development db:

```bin/console d:f:l -n"```

To stop all servers you must run (in the sf5_exp_api-docker folder):

```docker-compose compose stop```

That's all, now you can run the wonder and magnificent Expenses API via http://your_local_host_ip:200_or_domain_name/api/v1/docs or 
maybe you want to use a client like Postman.

### NGINX LOCAL CONFIGURATIONS

To get works all servers with custom urls you need to install a Nginx server and make these configurations:

#### HOSTS FILE EXAMPLE
Add to the hosts file (/etc/hosts) this pairs for example:

```
127.0.0.1       local.expensesapi.es
127.0.0.1       local.expapi-rabbitmq.es
127.0.0.1       local.expapi-mailhog.es
```

Then put the follow configs in your default.conf or do one for each server: 

#### API SERVER EXAMPLE

```
server {
  listen       80;
  server_name  local.expapi.es;

  location / {
    proxy_pass http://127.0.0.1:200;
  }
}
```

#### RABBIT MQ UI EXAMPLE

```
server {
  listen       80;
  server_name  local.expapi-rabbitmq.es;

  location / {
    proxy_pass http://127.0.0.1:15672;
  }
}
```

#### MAILHOG WEB UI EXAMPLE

```
server {
  listen       80;
  server_name  local.expapi-mailhog.es;

  location / {
    proxy_pass http://127.0.0.1:8025;
  }
}
```

Now you have nice urls and no more localhost:xxx or 127.0.0.1:xxx, it's much better.

### HOW USE THE API

There are two or three things to keep in mind, to use this API, there are two ROLES, administrator and 
normal user. The first user registered in the system will be an administrator role and the rest, a normal user role.

The administrator can do things like:

- Change users role
- Change user active status
- Get all data
- Change or delete all data except user data

The normal users only can change or delete their own data, or group, category, expense data if belongs to them.

Once registered in the system you can attack to the different endpoints only if you get login first, to do that, you need
to call to http://xxx.xxx.xxx/api/v1/login_check endpoint with an x-www-form-urlencoded form and _email and _password fields (with the info that
you set up in the register endpoint). 

## CAN I CHECK THE API ONLINE?

Of course, yes you can, we have deployed the project in a vps in the cloud, and you can check it online in the follow urls:

- API: https://exp-api.samsoft.es/api/v1/docs
- RabbitMQ:  https://exp-api-rabbitmq.samsoft.es/ (user: guest, pass: guest)
- MailHog: https://exp-api-mailhog.samsoft.es

BUT REMEMBER, **all data will be erased two times a day**, all what you do will be removed, this is only for testing and learning purposes.

## THERE IS EXTRA DOCUMENTATION

Yeah, you can check it in the following link, **but it's in Spanish language**, [documentation folder link](https://nextcloud.samsoft.es/s/TgRKwEcbpxwg4tg) .

That's all folks, see you soon, bye!!!
